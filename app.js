var express = require('express');

var app = express();

const PORT = process.env.PORT || 8080;

var router = express.Router();

router.get('/', function (request, response) {
  response.send('Helo, Wellcome!');
});

router.get('/student', function (request, response) {
  response.send('Wellcome, student!' );
});

router.get('/teacher', function (request, response) {
  response.send('Welcome, teacher!');
});

app.use('/', router);

app.listen(PORT, function () {
  console.log('Listening on port ' + PORT);
});
